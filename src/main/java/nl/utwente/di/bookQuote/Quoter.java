package nl.utwente.di.bookQuote;

public class Quoter
{
    public double getDegreesFahrenheit(String isbn)
    {
        double celcius = Double.parseDouble(isbn.replaceAll("[^\\d.]", ""));
        double fahrenheit = (celcius * 1.8) + 32;
        return fahrenheit;
    }
}
